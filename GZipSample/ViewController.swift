//
//  ViewController.swift
//  GZipSample
//
//  Created by Oka Yuya on 2017/08/04.
//  Copyright © 2017年 Oka Yuya. All rights reserved.
//

import UIKit
import Gzip
import MessageUI

class ViewController: UIViewController {

    fileprivate var zippedData: Data?

    override func viewDidLoad() {
        super.viewDidLoad()

        do {
            let data = try Data(contentsOf: URL(string: "https://dl.nnsnodnb.moe/images/hanayo.png")!, options: .dataReadingMapped)
            let compressedData = try data.gzipped()
            if compressedData.isGzipped {
                zippedData = compressedData
            }
        } catch {
            fatalError()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let mailViewController = MFMailComposeViewController()

        mailViewController.delegate = self
        mailViewController.setSubject("本文")
        mailViewController.setToRecipients(["oka.03_05@me.com"])
        mailViewController.setMessageBody("本文", isHTML: false)
        guard zippedData != nil else {
            return
        }
        mailViewController.addAttachmentData(zippedData!, mimeType: "application/zip", fileName: "image.zip")
        present(mailViewController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - MFMailComposeViewControllerDelegate

extension ViewController : MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        guard error != nil else {
            return
        }
        dismiss(animated: true, completion: nil)
        switch result {
        case .cancelled:
            print("Cancelled")
        case .saved:
            print("Saved")
        case .sent:
            print("Sent")
        case .failed:
            print("Failed")
        }
    }
}

// MARK : - UINavigationControllerDelegate

extension ViewController : UINavigationControllerDelegate {}
